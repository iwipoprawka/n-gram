﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGram
{
    public class NGram
    {
        int _startOrder = 2;
        int _maxOrder = 2;

        SortedList<string, int> ngrams = new SortedList<string, int>();
        int ngramCounter = 0;
        Dictionary<int, int> ngramsCount = new Dictionary<int, int>();

        public void AddToNGram(string word)
        {
            for (int order = _startOrder; order <= _maxOrder; ++order)
            {
                for (int i = 0; i < (word.Length - order + 1); i++)
                {
                    String key = word.Substring(i, order);

                    //Console.Write(".");
                    //Console.WriteLine(i + " " + (i + order) + " ");
                    //Console.WriteLine(key);

                    if (!ngrams.ContainsKey(key))
                    {
                        //if ngram don't exist in dictionary it also don't exist in our
                        //article stats, so we can add to article stats ngram and count 1
                        ngrams.Add(key, ngramCounter);
                        ngramsCount.Add(ngramCounter, 1);
                        //we add ngram id and initialize it count to create statistics 
                        //ngram <-> count of articles in which this ngram occure
                        ngramCounter++;
                    }
                    else
                    {
                        //if our stats already contains this ngram, we must update its count
                        if (ngramsCount.ContainsKey(ngrams[key]))
                        {
                            int tmpCount = ngramsCount[ngrams[key]] + 1;
                            ngramsCount.Remove(ngrams[key]);
                            ngramsCount.Add(ngrams[key], tmpCount);
                        }
                        //and if ngram don't exist in article stats - we add it with count 1
                        else
                        {
                            ngramsCount.Add(ngrams[key], 1);
                            //we update ngram count
                            //ngram <-> count of articles in which this ngram occure
                        }
                    }
                }
            }


        }

        public void setMin(int i)
        {
            _startOrder = i;
        }

        public void setMax(int i)
        {
            _maxOrder = i;
        }

        public void saveToCSV()
        {
            string filePath = @"C:\Downloads\test.csv";
            string delimiter = ",";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
            else
            {
                File.Delete(filePath);
                File.Create(filePath).Close();
            }
            StringBuilder sb = new StringBuilder();

            foreach (var ngram in ngrams)
            {
                sb.AppendLine(ngram.Key+","+ngramsCount[ngram.Value]);
            }
            //Console.WriteLine(sb);
            File.AppendAllText(filePath, sb.ToString());
        }



    }
}
