﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGram;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PROGRAM START");
            NGram.NGram angielski = new NGram.NGram();

            string path = @"C:\Downloads\ipa.txt";

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string line = sr.ReadLine();
                    string[] words = line.Split();
                    foreach (string word in words)
                    {
                        angielski.AddToNGram(word.ToLower());
                    }
                }
            }


            angielski.saveToCSV();

            Console.ReadLine();
        }

       

    }
}
